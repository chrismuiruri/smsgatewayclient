package www.smsgateway.ke.smsgateway.gcm;


public class Util {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String USER_NAME = "user_name";
    public static final String EMAIL = "email";

    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public final static String SENDER_ID = "7628665207";

    public static String base_url = "http://projects.newworkscomputers.com/gcm_demo/";

    public final static String  register_url=base_url+"register.php";
    public final static String  send_chat_url=base_url+"sendChatmessage.php";

}
