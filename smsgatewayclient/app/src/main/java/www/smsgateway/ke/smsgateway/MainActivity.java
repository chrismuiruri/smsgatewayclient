package www.smsgateway.ke.smsgateway;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import www.smsgateway.ke.smsgateway.gcm.ChatActivity;
import www.smsgateway.ke.smsgateway.gcm.GcmHomeActivity;
import www.smsgateway.ke.smsgateway.gcm.Util;
import www.smsgateway.ke.smsgateway.utils.Preferences;

/*
* this is here to get u started.
* */

public class MainActivity extends AppCompatActivity {

    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = new Preferences();

        //launch the chat activity if the user is already registered
        if (preferences.getPreferences(getApplicationContext(), "USER_DETAILS", Util.MOBILE_NUMBER) != null) {
            startActivity(new Intent(MainActivity.this, ChatActivity.class));
            finish();
        }

        Button startGcm = (Button) findViewById(R.id.start_gcm_btn);
        startGcm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GcmHomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
