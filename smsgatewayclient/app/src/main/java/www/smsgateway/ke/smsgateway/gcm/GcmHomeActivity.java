package www.smsgateway.ke.smsgateway.gcm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import www.smsgateway.ke.smsgateway.R;
import www.smsgateway.ke.smsgateway.utils.Preferences;
import www.smsgateway.ke.smsgateway.utils.VolleyApplication;

/*
*
* By Chris Muiruri N.
* so here is where registration happens
* */

public class GcmHomeActivity extends AppCompatActivity {

    EditText editText_user_name,editText_email;
    Button button_login;
    static final String TAG = "www.smsgateway.ke.smsgateway";
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    Context context;
    String registration_id,msg;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gcm_home);

        preferences = new Preferences();
        context = getApplicationContext();

        //check if user is registered
        if (isUserRegistered(context)) {
            startActivity(new Intent(GcmHomeActivity.this, ChatActivity.class));
            finish();
        } else {

            editText_user_name = (EditText) findViewById(R.id.editText_user_name);
            editText_email = (EditText) findViewById(R.id.editText_email);
            button_login = (Button) findViewById(R.id.button_login);

            button_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendRegistrationIdToBackend(); //send user details to the server for registration
                }
            });

            // Check device for Play Services APK. If check succeeds, proceed with
            //  GCM registration.
            if (checkPlayServices()) {
                gcm = GoogleCloudMessaging.getInstance(this);
                registration_id = getRegistrationId(context);
                if (registration_id.isEmpty()) {
                    registerInBackground(); //get a registration_id
                }
            } else {
                Log.i("smsgatewway", "No valid Google Play Services APK found.");
            }
        }
    }

    //checks if goofle play services are available
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Util.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    //returns the registration id stored locally
    public String getRegistrationId(Context context) {
        if (preferences.getPreferences(context, "USER_DETAILS", Util.PROPERTY_REG_ID) == null) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = preferences.getIntPreferences(context, "USER_DETAILS", Util.PROPERTY_APP_VERSION);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return preferences.getPreferences(context, "USER_DETAILS", Util.PROPERTY_REG_ID);
    }

    //returns true if user has already registered
    public boolean isUserRegistered(Context context) {
        if (preferences.getPreferences(context, "USER_DETAILS", Util.MOBILE_NUMBER) == null) {
            Log.i(TAG, "Registration not found.");
            return false;
        }
        return true;
    }

    //returns the current app version
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    //save the registration id locally
    public void storeRegistrationId(Context context, String regId) {
        int appVersion = getAppVersion(context);
        preferences.storePreferences(context, "USER_DETAILS", Util.PROPERTY_REG_ID, regId);
        preferences.storePreferences(context, "USER_DETAILS", Util.PROPERTY_APP_VERSION, appVersion);
        Log.i(TAG, "Saving regId on app version " + appVersion);
    }

    //store the user details locally
    public void storeUserDetails(Context context) {
        int appVersion = getAppVersion(context);
        preferences.storePreferences(context, "USER_DETAILS", Util.USER_NAME, editText_user_name.getText().toString());
        preferences.storePreferences(context, "USER_DETAILS", Util.MOBILE_NUMBER, editText_email.getText().toString());
        Log.i(TAG, "Saving regId on app version " + appVersion);
    }

    //NOW SEND USER DETAILS TO THE SERVER
    //I AM USING VOLLEY LIBRARY
    private void sendRegistrationIdToBackend() {

        //make sure user inputs are not empty
        //in a production scenerio, the user mobile_number should be verified using something like a text message
        if (editText_email.getText().toString().equals("") || editText_user_name.getText().toString().equals("")) {
            Toast.makeText(GcmHomeActivity.this, "Wacha mchezo!", Toast.LENGTH_LONG).show();
            return;
        }

        String url = Util.register_url + "?username=" + editText_user_name.getText().toString() + "&mobile_number=" + editText_email.getText().toString() + "&registration_id=" + registration_id;
        url = url.replace(" ", "%20");
        String tag_json_obj = "json_obj_obj"; /* tag used to cancel the request */

        /* show a progressDialog */
        final ProgressDialog progressDialog = new ProgressDialog(GcmHomeActivity.this,
                R.style.Base_Theme_AppCompat_Light_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        TextView progressDialogTv = (TextView) progressDialog.findViewById(android.R.id.message);
        progressDialogTv.setTextColor(getResources().getColor(android.R.color.black));
        progressDialogTv.setPadding(10, 10, 10, 10);
        progressDialogTv.setText("Authenticating...");

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.equals("success")) {
                            storeUserDetails(context); //store the details locally
                            progressDialog.dismiss(); // remove the dialog
                            startActivity(new Intent(GcmHomeActivity.this, ChatActivity.class));
                            finish(); //start ChatActivity
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Try Again" + response, Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(GcmHomeActivity.this, "Response error" + error.toString(), Toast.LENGTH_LONG).show();

            }
        });

        /* EXTENDS THE REQUEST TIMEOUT */
        int socketTimeout = 20000;// 20 secs
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

         /* add the request to the request queue */
        VolleyApplication.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

    //get a registration id from GCM
    private void registerInBackground() {
        new AsyncTask<Object, Void, String>() {
            @Override
            protected String doInBackground(Object... objects) {
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(GcmHomeActivity.this);
                    }
                    registration_id = gcm.register(Util.SENDER_ID);
                    msg = "Device registered, registration ID=" + registration_id;
                    storeRegistrationId(context, registration_id);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }
        }.execute();
    }
}
