package www.smsgateway.ke.smsgateway.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import www.smsgateway.ke.smsgateway.R;
import www.smsgateway.ke.smsgateway.utils.Preferences;
import www.smsgateway.ke.smsgateway.utils.VolleyApplication;

/*
* The chat activity
* */

public class ChatActivity extends AppCompatActivity {

    EditText editText_mail_id,editText_chat_message;
    ListView listView_chat_messages;
    Button button_send_chat;
    List<ChatObject> chat_list;
    BroadcastReceiver receive_chat;
    Preferences preferences;
    String userMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        preferences = new Preferences();
        userMobile = preferences.getPreferences(getApplicationContext(), "USER_DETAILS", Util.MOBILE_NUMBER);

        editText_mail_id = (EditText) findViewById(R.id.editText_mail_id);
        editText_chat_message = (EditText) findViewById(R.id.editText_chat_message);
        listView_chat_messages = (ListView) findViewById(R.id.listView_chat_messages);
        button_send_chat = (Button) findViewById(R.id.button_send_chat);
        button_send_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // send chat message to server
                String message = editText_chat_message.getText().toString();
                showChat("sent", message);
                sendMessage();
                editText_chat_message.setText("");

            }
        });

        receive_chat = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String from = intent.getStringExtra("from");
                String message = intent.getStringExtra("message");
                Log.d("www.smsgateway.ke.smsgateway", "in local braod " + message + ", from" + from + ", User Mobile " + userMobile);
                showChat("recieve", message + " (" + from + ")");
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(receive_chat, new IntentFilter("message_recieved"));
    }

    /* am overidding onNewIntent just to be sure */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!intent.getStringExtra("message").equals("")) {
            receive_chat = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String from = intent.getStringExtra("from");
                    String message = intent.getStringExtra("message");
                    Log.d("pavan", "in local broadcast " + message + ", from" + from + ", User Mobile " + userMobile);
                    showChat("recieve", message + " (" + from + ")");
                }
            };
            LocalBroadcastManager.getInstance(this).registerReceiver(receive_chat, new IntentFilter("message_recieved"));
        }
    }

    private void showChat(String type, String message) {
        if (chat_list == null || chat_list.size() == 0) {
            chat_list = new ArrayList<ChatObject>();
        }
        chat_list.add(new ChatObject(message, type));
        ChatAdabter chatAdabter = new ChatAdabter(ChatActivity.this, R.layout.chat_view, chat_list);
        listView_chat_messages.setAdapter(chatAdabter);
        chatAdabter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void sendMessage() {
        String url = Util.send_chat_url + "?mobile_number=" + editText_mail_id.getText().toString() + "&message=" + editText_chat_message.getText().toString() + "&senderNumber=" + userMobile;

        url = url.replace(" ", "%20");
        String tag_json_obj = "json_obj_obj"; /* tag used to cancel the request */

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //do something
                        //Toast.makeText(ChatActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChatActivity.this, "Message not sent: " + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        /* EXTENDS THE REQUEST TIMEOUT */
        int socketTimeout = 20000;// 20 secs
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

         /* add the request to the request queue */
        VolleyApplication.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

}
